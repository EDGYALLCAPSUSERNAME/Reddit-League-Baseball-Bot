import praw
import dataset
import re
import time

REDDIT_USERNAME = ''
REDDIT_PASS = ''

USER_TO_REPLY_TO = ''
BOT_IDENTIFIER = ''
COMMENT_REPLY = '/u/{} now has {} points.'
WIKI_STRING_TEMPLATE = "/u/{}: {} points\n"

SUBREDDIT_NAME = ''
WIKI_PAGE_NAME = ''

TOP_WIKI_STRING = '''
This will go above all the users points.\n
'''

BOTTOM_WIKI_STRING = '''
This will got underneath all the users points.\n
'''

def add_scores(comment, db, r):
    if re.search(BOT_IDENTIFIER, str(comment), re.IGNORECASE):
        try:
            points = int(str(comment).split(' ')[1])
        except ValueError:
            print "Value Error, can't convert to interger"
            return
        except IndexError:
            print "Comment is too short, can't do anything. Continuing..."
            return
            
        author = str(r.get_info(thing_id=comment.parent_id).author)
        user =  db['users'].find_one(username = author)
        if user != None:
            new_points = user['total_points'] + points
            print "Updating users points..."
            data = dict(id = user['id'], total_points = new_points)
            db['users'].update(data, ['id'])
            print "Replying with update..."
            # gets the updated point value
            user =  db['users'].find_one(username = author)
            comment.reply(COMMENT_REPLY.format(author, user['total_points']))
            data = dict(comment_id = str(comment.id))
            db['replied_to'].insert(data)
        else:
            print "Creating a new user and adding points..."
            data = dict(username = author, total_points = points)
            db['users'].insert(data)
            print "Replying..."
            comment.reply(COMMENT_REPLY.format(author, points))
            data = dict(comment_id = str(comment.id))
            db['replied_to'].insert(data)
    else:
        return

def update_wiki(db, r):
    wiki_string = ''
    for user in db['users']:
        wiki_string += WIKI_STRING_TEMPLATE.format(user['username'], user['total_points'])

    print "Updating wiki..."
    wiki_page = TOP_WIKI_STRING + wiki_string + BOTTOM_WIKI_STRING
    r.edit_wiki_page(SUBREDDIT_NAME, WIKI_PAGE_NAME, wiki_page, 'update')

def main():
    print "Logging in..."
    r = praw.Reddit(user_agent = 'Reddit League Baseball    v0.1')
    r.login(REDDIT_USERNAME, REDDIT_PASS, disable_warning = True)
    user = r.get_redditor(USER_TO_REPLY_TO)
    db = dataset.connect('sqlite:///rlb.db')

    while True:
        comments = user.get_comments(sort = 'new', limit = 50)

        for comment in comments:
            if db['replied_to'].find_one(comment_id = str(comment.id)) == None:
                add_scores(comment, db, r)

        update_wiki(db, r)

        # currently waits 5 minutes before checking again
        # adjust this if you want it to wait longer or shorter
        # time is in seconds
        time.sleep(300)

if __name__ == "__main__":
    main()
