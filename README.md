Reddit League Baseball Bot
==========================

Setup
-----

This script requires python 2.7 and [praw](http://praw.readthedocs.org/en/), [dataset](https://dataset.readthedocs.org), and [sqlalchemy](http://docs.sqlalchemy.org/en/rel_1_0/intro.html).

To install praw type in your command line:

    pip install praw

To install dataset:

    pip install dataset

to install sqlalchemy:

    pip install SQLAlchemy

Before running reddit_league_baseball.py for the first time, you must run create_tables.py, this script
will create a new db file, and setup the tables that are used in reddit_league_baseball.py. You only need to
run this once.

You must add an identifier to the bot, so it knows which comments to reply to,
replace the BOT_IDENTIFIER with whatever you want to call it. Then all comments you
want the bot to look at and reply to must follow the template:

    BOT_IDENTIFIER +INTEGER

Then open up reddit_league_baseball.py and add the reddit username and password
for the bot. Then add the user that you want the bot to search comments and reply to.
Add the subreddit name to the SUBREDDIT_NAME variable, without the leading /r/. And add
the name of the wiki page you want it to edit (i.e. index). If the page doesn't already
exist, it will be created.

Additional Information
----------------------

There are two strings that you can edit to change the reply and wiki entries.
The first is COMMENT_REPLY. The default is:

    '/u/{} now has {} points.'

You can edit this however you like, but the two sets of brackets must stay. The first set of brackets gets
replaced with the username of the user who is awarded the points, and the second set of
brackets is the amount of points they are awarded. So keep that in mind if you choose to
edit it.

The second is the wiki entries. The variable is WIKI_STRING_TEMPLATE and the default is:

    '/u/{}: {} points\n'

As with the comment reply, the first is the username and second is the
total amount of points. The last is the newline character which must stay at the end of the string
if you want each wiki entry to be separated with line breaks.
