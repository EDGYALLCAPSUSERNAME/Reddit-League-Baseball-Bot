import dataset
import sqlalchemy

db = dataset.connect('sqlite:///rlb.db')
print "Creating users table..."
db.create_table('users')
print "Creating replied to table..."
db.create_table('replied_to')

users_table_columns = {'username': sqlalchemy.String,
                       'total_points': sqlalchemy.Integer}

print "Adding columns to users..."
for c_name, c_type in users_table_columns.iteritems():
    db['users'].create_column(c_name, c_type)

replied_to_columns = {'comment_id': sqlalchemy.String}

print "Adding columns to replied_to..."
for c_name, c_type in replied_to_columns.iteritems():
    db['replied_to'].create_column(c_name, c_type)
